<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StudentsProfessors Controller
 *
 * @property \App\Model\Table\StudentsProfessorsTable $StudentsProfessors
 *
 * @method \App\Model\Entity\StudentsProfessor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StudentsProfessorsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students', 'Professors']
        ];
        $studentsProfessors = $this->paginate($this->StudentsProfessors);

        $this->set(compact('studentsProfessors'));
    }

    /**
     * View method
     *
     * @param string|null $id Students Professor id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $studentsProfessor = $this->StudentsProfessors->get($id, [
            'contain' => ['Students', 'Professors']
        ]);

        $this->set('studentsProfessor', $studentsProfessor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $studentsProfessor = $this->StudentsProfessors->newEntity();
        if ($this->request->is('post')) {
            $studentsProfessor = $this->StudentsProfessors->patchEntity($studentsProfessor, $this->request->getData());
            if ($this->StudentsProfessors->save($studentsProfessor)) {
                $this->Flash->success(__('The students professor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The students professor could not be saved. Please, try again.'));
        }
        $students = $this->StudentsProfessors->Students->find('list', ['limit' => 200]);
        $professors = $this->StudentsProfessors->Professors->find('list', ['limit' => 200]);
        $this->set(compact('studentsProfessor', 'students', 'professors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Students Professor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $studentsProfessor = $this->StudentsProfessors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $studentsProfessor = $this->StudentsProfessors->patchEntity($studentsProfessor, $this->request->getData());
            if ($this->StudentsProfessors->save($studentsProfessor)) {
                $this->Flash->success(__('The students professor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The students professor could not be saved. Please, try again.'));
        }
        $students = $this->StudentsProfessors->Students->find('list', ['limit' => 200]);
        $professors = $this->StudentsProfessors->Professors->find('list', ['limit' => 200]);
        $this->set(compact('studentsProfessor', 'students', 'professors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Students Professor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $studentsProfessor = $this->StudentsProfessors->get($id);
        if ($this->StudentsProfessors->delete($studentsProfessor)) {
            $this->Flash->success(__('The students professor has been deleted.'));
        } else {
            $this->Flash->error(__('The students professor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
