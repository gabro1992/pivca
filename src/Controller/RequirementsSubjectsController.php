<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RequirementsSubjects Controller
 *
 * @property \App\Model\Table\RequirementsSubjectsTable $RequirementsSubjects
 *
 * @method \App\Model\Entity\RequirementsSubject[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RequirementsSubjectsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Requirements', 'Subjects']
        ];
        $requirementsSubjects = $this->paginate($this->RequirementsSubjects);

        $this->set(compact('requirementsSubjects'));
    }

    /**
     * View method
     *
     * @param string|null $id Requirements Subject id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $requirementsSubject = $this->RequirementsSubjects->get($id, [
            'contain' => ['Requirements', 'Subjects']
        ]);

        $this->set('requirementsSubject', $requirementsSubject);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $requirementsSubject = $this->RequirementsSubjects->newEntity();
        if ($this->request->is('post')) {
            $requirementsSubject = $this->RequirementsSubjects->patchEntity($requirementsSubject, $this->request->getData());
            if ($this->RequirementsSubjects->save($requirementsSubject)) {
                $this->Flash->success(__('The requirements subject has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The requirements subject could not be saved. Please, try again.'));
        }
        $requirements = $this->RequirementsSubjects->Requirements->find('list', ['limit' => 200]);
        $subjects = $this->RequirementsSubjects->Subjects->find('list', ['limit' => 200]);
        $this->set(compact('requirementsSubject', 'requirements', 'subjects'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Requirements Subject id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $requirementsSubject = $this->RequirementsSubjects->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $requirementsSubject = $this->RequirementsSubjects->patchEntity($requirementsSubject, $this->request->getData());
            if ($this->RequirementsSubjects->save($requirementsSubject)) {
                $this->Flash->success(__('The requirements subject has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The requirements subject could not be saved. Please, try again.'));
        }
        $requirements = $this->RequirementsSubjects->Requirements->find('list', ['limit' => 200]);
        $subjects = $this->RequirementsSubjects->Subjects->find('list', ['limit' => 200]);
        $this->set(compact('requirementsSubject', 'requirements', 'subjects'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Requirements Subject id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $requirementsSubject = $this->RequirementsSubjects->get($id);
        if ($this->RequirementsSubjects->delete($requirementsSubject)) {
            $this->Flash->success(__('The requirements subject has been deleted.'));
        } else {
            $this->Flash->error(__('The requirements subject could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
