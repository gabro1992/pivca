<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DetalleCobros Controller
 *
 * @property \App\Model\Table\DetalleCobrosTable $DetalleCobros
 *
 * @method \App\Model\Entity\DetalleCobro[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DetalleCobrosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cobros']
        ];
        $detalleCobros = $this->paginate($this->DetalleCobros);

        $this->set(compact('detalleCobros'));
    }

    /**
     * View method
     *
     * @param string|null $id Detalle Cobro id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $detalleCobro = $this->DetalleCobros->get($id, [
            'contain' => ['Cobros']
        ]);

        $this->set('detalleCobro', $detalleCobro);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $detalleCobro = $this->DetalleCobros->newEntity();
        if ($this->request->is('post')) {
            $detalleCobro = $this->DetalleCobros->patchEntity($detalleCobro, $this->request->getData());
            if ($this->DetalleCobros->save($detalleCobro)) {
                $this->Flash->success(__('The detalle cobro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The detalle cobro could not be saved. Please, try again.'));
        }
        $cobros = $this->DetalleCobros->Cobros->find('list', ['limit' => 200]);
        $this->set(compact('detalleCobro', 'cobros'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Detalle Cobro id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $detalleCobro = $this->DetalleCobros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $detalleCobro = $this->DetalleCobros->patchEntity($detalleCobro, $this->request->getData());
            if ($this->DetalleCobros->save($detalleCobro)) {
                $this->Flash->success(__('The detalle cobro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The detalle cobro could not be saved. Please, try again.'));
        }
        $cobros = $this->DetalleCobros->Cobros->find('list', ['limit' => 200]);
        $this->set(compact('detalleCobro', 'cobros'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Detalle Cobro id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $detalleCobro = $this->DetalleCobros->get($id);
        if ($this->DetalleCobros->delete($detalleCobro)) {
            $this->Flash->success(__('The detalle cobro has been deleted.'));
        } else {
            $this->Flash->error(__('The detalle cobro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
