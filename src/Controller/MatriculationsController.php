<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;

/**
 * Matriculations Controller
 *
 * @property \App\Model\Table\MatriculationsTable $Matriculations
 *
 * @method \App\Model\Entity\Matriculation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MatriculationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Careers', 'Students','Subjects']
        ];
        $matriculations = $this->paginate($this->Matriculations);

        $this->set(compact('matriculations'));
    }

    /**
     * View method
     *
     * @param string|null $id Matriculation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $matriculation = $this->Matriculations->get($id, [
            'contain' => ['Careers', 'Subjects']
        ]);

        $this->set('matriculation', $matriculation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Estándar PSR-7 debug(json_encode($dependencies,JSON_PRETTY_PRINT));
        $matriculation = $this->Matriculations->newEntity();
        if ($this->request->is('post')) {
            ###########################
            $this->loadModel('Students');
            $this->loadModel('Subjects');
            $this->loadModel('Dependencies');

            $subject_ids = '';

            $student = $this->Students->get($this->request->getData()['student_id'], [
                'contain' => ['Matriculations'=> ['Subjects']]
            ]);

            $cantidadMatriculaciones = count($student->matriculations);

            if ($cantidadMatriculaciones === 0) {
                # Es la primera vez que se matricula el estudiante
                foreach ($this->request->getData()['subjects']['_ids'] as $keyDependencias=> $valueDependencias) {
                    $dependencies[$keyDependencias] = $this->Dependencies->find('all', [
                        'conditions'=>['subject_id'=>$valueDependencias]
                    ]);

                    foreach ($dependencies as $keyhijomateria => $valuehijomateria) {
                        foreach ($valuehijomateria as $keyIdHijoMateria => $valueIdHijoMateria) {
                            // Esto debe optimizarse con el array_search
                            if ($valueIdHijoMateria->hijomateria > 0) {
                                $this->Flash->error(__('The matriculation could not be saved. Please, try again.'));
                            }else {
                                # Código para matricularse
                                if ($this->Matriculations->save($matriculation)) {
                                    //Pendiente
                                /* $fee = $this->Fees->newEntity();
                                    $fee->total = 23;
                                    $fee->matriculation_id = $matriculation->id;
                                    $this->Fees->save($fee);*/
                                    $this->Flash->success(__('The matriculation has been saved.'));

                                    return $this->redirect(['action' => 'index']);
                                }
                                $this->Flash->error(__('The matriculation could not be saved. Please, try again.'));
                            }
                        }
                    }
                }
            } else {
                # Ya se ha matriculado alguna vez
                $matriculaciones = ($student->matriculations);
                $cont2 = 0;
                foreach ($matriculaciones as $keyMatriculacion => $valueMatriculacion) {
                    foreach ($valueMatriculacion->subjects as $cont => $valueMas) {
                        $materias_matriculadas[$cont2]['id'] =  $valueMas['_joinData']['subject_id'];
                        $materias_matriculadas[$cont2]['aprobada'] = $valueMas['_joinData']['approved'];
                        $cont2++;
                    }
                }

                foreach ($this->request->getData()['subjects']['_ids'] as $mierda=> $value) {
                    $dependencias[] = $this->Dependencies->find('all', [
                        'conditions'=>['subject_id'=>$value]
                    ]);
                }

                foreach ($dependencias as $key => $valuesubjects) {

                    foreach ($valuesubjects as $keyVer => $valueVer) {
                        $materiasPreRequisitos[] = json_encode($valueVer->hijomateria,JSON_PRETTY_PRINT);
                    }
                }
               # $materiasPreRequisitos contiene  los preRequisitos de la materia a matricularse.
                # $materiasPreRequisitos contiene  los preRequisitos de la materia a matricularse, $materias_matriculadas contiene  las materias matriculadas ya del estudiante, entonces dentro de $materias_matriculadas se buscan las $materiasPreRequisitos, si no lo encuentra significa que el alumno aún no curso la materia requerido y debe impedir la matriculación, si encuentra la materia pero no está aprobada tampoco debe matricularse.
                $matricularse = true;
                foreach ($materiasPreRequisitos as $keyMateriasPreRequisitos => $valueMateriasPreRequisitos) {
                   if(!$valueMateriasPreRequisitos){
                       # Significa que no tiene preRequisitos
                        $matricularse = true;
                   }else
                   {
                       //Si existe la materia dentro del array devuelve la posición de la misma, o false en caso contrario
                        $idMateria  = array_search($valueMateriasPreRequisitos, array_column($materias_matriculadas, 'id'));
                        if($idMateria === false){
                            # No ha cursado el preRequisito.
                            $matricularse = false;
                            break;
                        }else{
                            # ya lo ha cursado y toca verificar si tiene la materia aprobada
                            if($materias_matriculadas[$idMateria]['aprobada']){
                                $matricularse = true;
                            }else{
                                $matricularse = false;
                                break;
                            }
                        }
                   }
                }
                ###########################
                if ($matricularse) {
                    //Se carga el modelo Fees
                    $this->loadModel('Fees');
                    //Se obtiene la cantidad de asignaturas a matricular, se crea y asigna dicha cantidad al array en su indice quantity_subjects
                    $matriculation['quantity_subjects'] = count($this->request->getData()['subjects']['_ids']);
                    $matriculation = $this->Matriculations->patchEntity($matriculation, $this->request->getData());
                    if ($this->Matriculations->save($matriculation)) {
                        //Pendiente
                    /* $fee = $this->Fees->newEntity();
                        $fee->total = 23;
                        $fee->matriculation_id = $matriculation->id;
                        $this->Fees->save($fee);*/
                        $this->Flash->success(__('The matriculation has been saved.'));

                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The matriculation could not be saved. Please, try again.'));
                } else {
                    $this->Flash->error(__('The matriculation could not be saved. Please, try again.'));
                }
            }
        }
        $careers = $this->Matriculations->Careers->find('list', ['limit' => 200]);
        $subjects = $this->Matriculations->Subjects->find('list', ['limit' => 200]);
        $students = $this->Matriculations->Students->find('list', ['limit' => 200]);
        $this->set(compact('matriculation', 'careers', 'subjects','students'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Matriculation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        /*debug('test');
        debug($this->request->getData());die();*/
        //debug(($this->request->getData()['matricula_id']));
        //debug($this->request->getData());die();


        $matriculation = $this->Matriculations->get($id, [
            'contain' => ['Subjects']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //debug('test');die();
            $matriculation = $this->Matriculations->patchEntity($matriculation, $this->request->getData());
            if ($this->Matriculations->save($matriculation)) {
                $this->Flash->success(__('The matriculation has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The matriculation could not be saved. Please, try again.'));
        }
        $careers = $this->Matriculations->Careers->find('list', ['limit' => 200]);
        $subjects = $this->Matriculations->Subjects->find('list', ['limit' => 200]);
        $this->set(compact('matriculation', 'careers', 'subjects'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Matriculation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->autoRender=false;
        $this->request->allowMethod(['post', 'delete']);
        $matriculation = $this->Matriculations->get($this->request->getData());

        try {
            $this->Matriculations->delete($matriculation);
            $this->response->body(json_encode(true));
            return $this->response;
        } catch (\PDOException $e) {
            $this->response->body(json_encode(false));
        }
    }

    public function aprobar()
    {
        $this->autoRender=false;
        $this->loadModel('SubjectsMatriculation');
        $subjectsMatriculation = TableRegistry::get('SubjectsMatriculations');
        $query = $subjectsMatriculation->query();
        $query->update()
        ->set(['approved' => 1])
        ->where(['subject_id' => $this->request->getData()['subject_id'],'matriculation_id' => $this->request->getData()['matricula_id']])
        ->execute();
        $this->response->body(json_encode(true));
        return $this->response;
    }
}
