<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SubjectsMatriculations Controller
 *
 * @property \App\Model\Table\SubjectsMatriculationsTable $SubjectsMatriculations
 *
 * @method \App\Model\Entity\SubjectsMatriculation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubjectsMatriculationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Subjects', 'Matriculations']
        ];
        $subjectsMatriculations = $this->paginate($this->SubjectsMatriculations);

        $this->set(compact('subjectsMatriculations'));
    }

    /**
     * View method
     *
     * @param string|null $id Subjects Matriculation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subjectsMatriculation = $this->SubjectsMatriculations->get($id, [
            'contain' => ['Subjects', 'Matriculations']
        ]);

        $this->set('subjectsMatriculation', $subjectsMatriculation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subjectsMatriculation = $this->SubjectsMatriculations->newEntity();
        if ($this->request->is('post')) {
            $subjectsMatriculation = $this->SubjectsMatriculations->patchEntity($subjectsMatriculation, $this->request->getData());
            if ($this->SubjectsMatriculations->save($subjectsMatriculation)) {
                $this->Flash->success(__('The subjects matriculation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The subjects matriculation could not be saved. Please, try again.'));
        }
        $subjects = $this->SubjectsMatriculations->Subjects->find('list', ['limit' => 200]);
        $matriculations = $this->SubjectsMatriculations->Matriculations->find('list', ['limit' => 200]);
        $this->set(compact('subjectsMatriculation', 'subjects', 'matriculations'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Subjects Matriculation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subjectsMatriculation = $this->SubjectsMatriculations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subjectsMatriculation = $this->SubjectsMatriculations->patchEntity($subjectsMatriculation, $this->request->getData());
            if ($this->SubjectsMatriculations->save($subjectsMatriculation)) {
                $this->Flash->success(__('The subjects matriculation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The subjects matriculation could not be saved. Please, try again.'));
        }
        $subjects = $this->SubjectsMatriculations->Subjects->find('list', ['limit' => 200]);
        $matriculations = $this->SubjectsMatriculations->Matriculations->find('list', ['limit' => 200]);
        $this->set(compact('subjectsMatriculation', 'subjects', 'matriculations'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Subjects Matriculation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subjectsMatriculation = $this->SubjectsMatriculations->get($id);
        if ($this->SubjectsMatriculations->delete($subjectsMatriculation)) {
            $this->Flash->success(__('The subjects matriculation has been deleted.'));
        } else {
            $this->Flash->error(__('The subjects matriculation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
