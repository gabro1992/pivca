<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Matriculation $matriculation
 */
?>

<script>
$(document).ready(function() {
    $(document).on('click','.aprobarMateria',function(e) {
        e.preventDefault();
        var subject_id = $(this).attr('subject_id');
        var matricula_id = $(this).attr('matriculacion_id');
        $.ajax({
        method: "POST",
        url: "http://localhost/pivcaCake/matriculations/aprobar",
        headers : {'X-CSRF-Token': $('[name="_csrfToken"]').val()},
        data: { matricula_id: matricula_id, subject_id: subject_id},
        })
        .done(function( msg ) {
            if(msg == 'true'){

            }else{

            }
        });
    });
});
</script>>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
    <?php echo
    $this->Html->link(
             array(
                   'href' => '#',
                   'target' => '_blank',
                   'escape' => false
                   ),
             false     // Second argument, true means prepend the path of my site before the link while false means don't prepend
     ); ?>
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Matriculation'), ['action' => 'edit', $matriculation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Matriculation'), ['action' => 'delete', $matriculation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $matriculation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Matriculations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Matriculation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Careers'), ['controller' => 'Careers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Career'), ['controller' => 'Careers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="matriculations view large-9 medium-8 columns content">
    <h3><?= h($matriculation->description) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($matriculation->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Career') ?></th>
            <td><?= $matriculation->has('career') ? $this->Html->link($matriculation->career->name, ['controller' => 'Careers', 'action' => 'view', $matriculation->career->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($matriculation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Student Id') ?></th>
            <td><?= $this->Number->format($matriculation->student_id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Subjects') ?></h4>
        <?php if (!empty($matriculation->subjects)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Materia</th>
                <th scope="col">Descripción</th>
                <th scope="col">Id Carrera</th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
            <?php foreach ($matriculation->subjects as $subjects): ?>
            <tr>
                <?php //debug(json_encode($subjects),JSON_PRETTY_PRINT);
                //debug($subjects->_joinData['matriculation_id']);die();?>
                <td><?= h($subjects->id) ?></td>
                <td><?= h($subjects->name) ?></td>
                <td><?= h($subjects->description) ?></td>
                <td><?= h($subjects->career_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Subjects', 'action' => 'view', $subjects->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Subjects', 'action' => 'edit', $subjects->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Subjects', 'action' => 'delete', $subjects->id], ['confirm' => __('Are you sure you want to delete ?', $subjects->name)]) ?>
                    <?= $this->Html->link('<i class="fas fa-check"></i>', '#',['class'=>'btn btn-sm btn-primary aprobarMateria','subject_id'=>$subjects->_joinData['subject_id'],'matriculacion_id'=>$subjects->_joinData['matriculation_id'],'escape'=>false]) ?>
                    <?= $this->Html->link('<i class="fas fa-trash-alt"></i>', '#',['class'=>'btn btn-sm btn-danger borrar','escape'=>false,'matricula_id'=>2]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
