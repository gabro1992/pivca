<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Matriculation[]|\Cake\Collection\CollectionInterface $matriculations
 */
?>
<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        "pagingType": "full_numbers",
        "scrollX": true,
        "dom": 'Bfrtip',
        "buttons": [
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel"></i>',
                titleAttr: 'Excel',
                title: 'Data export'
            },
            {
                extend: 'pdfHtml5',
                title: 'Data export'
            }
        ]

    } );

    $(document).on('click','.borrar',function(e) {
        e.preventDefault();
        var vai = $(this).parents('tr');

        $.ajax({
        method: "POST",
        url: "matriculations/delete",
        headers : {
      'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        data: { matricula_id: $(this).attr("matricula_id")},
        })
        .done(function( msg ) {
            if(msg == 'true'){
                $('#example').DataTable()
                .row( vai )
                .remove()
                .draw();
                alert('Registro eliminado');
            }else{
                alert('NO SE PUEDE ELIMINAR');
            }


        });
       // $(this).unbind('click').click();
       // $(this).trigger('click');
    });
} );
</script>
<meta name="csrf-token" content="{{ csrf_token() }}">
<nav class="large-2 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Matriculation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Careers'), ['controller' => 'Careers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Career'), ['controller' => 'Careers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="matriculations index large-10 medium-8 columns content">

    <h3>Matriculationes</h3>
    <table class="table table-striped table-bordered" id="example">
        <thead>
            <tr>
                <th scope="col">CI</th>
                <th scope="col">Descripción</th>
                <th scope="col">Carrera</th>
                <th scope="col">Estudiante</th>
                <th scope="col">Materias</th>
                <th scope="col">Total Gs</th>
                <th width="20%">Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php

              //debug($matriculations);die();
              foreach ($matriculations as $matriculation):  ?>
            <tr>
                <td><?= $this->Number->format($matriculation->id) ?></td>
                <td><?= h($matriculation->description) ?></td>
                <td><?= $matriculation->has('career') ? $this->Html->link($matriculation->career->name, ['controller' => 'Careers', 'action' => 'view', $matriculation->career->id]) : '' ?></td>
                <td><?= $matriculation->has('student') ? $this->Html->link($matriculation->student->name, ['controller' => 'Students', 'action' => 'view', $matriculation->student->id]) : '' ?></td>
                <td>
                    <?php
                        //$this->Html->link('kola', ['controller' => 'Subjects', 'action' => 'view',1]);
                        //debug($matriculation->subjects);//die();
                        $i = 0;
                        foreach ($matriculation->subjects as $value_subjects) {

                           // debug($value_subjects->name);
                           if ($matriculation->has('subjects')) {
                               if ($i>0) {
                                   echo ', ';
                               }
                               echo $this->Html->link($value_subjects->name, ['controller' => 'Subjects', 'action' => 'view', $value_subjects->id]);
                           }else{
                               echo '';
                           }
                           $i++;
                        }
                       // die();
                    ?>
                </td>
                <td><?= ($matriculation->quantity_subjects)*20000 ?></td>
                <td class="actions">
                    <?= $this->Html->link(('Ver'), ['action' => 'view', $matriculation->id],['class'=>'btn btn-sm btn-primary ver']) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $matriculation->id],['class'=>'btn btn-sm btn-info']) ?>

                    <?= $this->Form->Postlink(__('Borrar'), '#', ['class'=>'d-none ']) ?>
                    <?= $this->Html->link('<i class="fas fa-trash-alt"></i>', '#',['class'=>'btn btn-sm btn-danger borrar','escape'=>false,'matricula_id'=>$matriculation->id]) ?>
                </td>
            </tr>
            <?php
                $i++;
                endforeach;
            ?>
        </tbody>
    </table>
</div>
