<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Matriculation $matriculation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Matriculations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Careers'), ['controller' => 'Careers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Career'), ['controller' => 'Careers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="matriculations form large-9 medium-8 columns content">
    <?= $this->Form->create($matriculation) ?>
    <fieldset>
        <legend><?= __('Add Matriculation') ?></legend>
        <?php
            echo $this->Form->control('description');
            echo $this->Form->control('career_id', ['options' => $careers]);
            echo $this->Form->control('student_id');
            echo $this->Form->control('subjects._ids', ['options' => $subjects]);
        ?>
    </fieldset>
    <?php //$this->Form->button(__('Submit')) ?>
    <button type="submit" class="btn btn-primary">Enviar</button>
    <?= $this->Form->end() ?>
</div>
