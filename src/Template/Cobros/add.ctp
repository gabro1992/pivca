<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cobro $cobro
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cobros'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Matriculations'), ['controller' => 'Matriculations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Matriculation'), ['controller' => 'Matriculations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Detalle Cobros'), ['controller' => 'DetalleCobros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Detalle Cobro'), ['controller' => 'DetalleCobros', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cobros form large-9 medium-8 columns content">
    <?= $this->Form->create($cobro) ?>
    <fieldset>
        <legend><?= __('Add Cobro') ?></legend>
        <?php
            echo $this->Form->control('total');
            echo $this->Form->control('matriculation_id', ['options' => $matriculations]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
