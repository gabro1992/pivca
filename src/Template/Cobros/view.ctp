<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cobro $cobro
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cobro'), ['action' => 'edit', $cobro->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cobro'), ['action' => 'delete', $cobro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cobro->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cobros'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cobro'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Matriculations'), ['controller' => 'Matriculations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Matriculation'), ['controller' => 'Matriculations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Detalle Cobros'), ['controller' => 'DetalleCobros', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Detalle Cobro'), ['controller' => 'DetalleCobros', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cobros view large-9 medium-8 columns content">
    <h3><?= h($cobro->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Matriculation') ?></th>
            <td><?= $cobro->has('matriculation') ? $this->Html->link($cobro->matriculation->description, ['controller' => 'Matriculations', 'action' => 'view', $cobro->matriculation->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cobro->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total') ?></th>
            <td><?= $this->Number->format($cobro->total) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Detalle Cobros') ?></h4>
        <?php if (!empty($cobro->detalle_cobros)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Materia') ?></th>
                <th scope="col"><?= __('Monto Cuota') ?></th>
                <th scope="col"><?= __('Cobro Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($cobro->detalle_cobros as $detalleCobros): ?>
            <tr>
                <td><?= h($detalleCobros->id) ?></td>
                <td><?= h($detalleCobros->materia) ?></td>
                <td><?= h($detalleCobros->monto_cuota) ?></td>
                <td><?= h($detalleCobros->cobro_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DetalleCobros', 'action' => 'view', $detalleCobros->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DetalleCobros', 'action' => 'edit', $detalleCobros->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DetalleCobros', 'action' => 'delete', $detalleCobros->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detalleCobros->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
