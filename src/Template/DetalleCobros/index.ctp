<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetalleCobro[]|\Cake\Collection\CollectionInterface $detalleCobros
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Detalle Cobro'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cobros'), ['controller' => 'Cobros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cobro'), ['controller' => 'Cobros', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="detalleCobros index large-9 medium-8 columns content">
    <h3><?= __('Detalle Cobros') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('materia') ?></th>
                <th scope="col"><?= $this->Paginator->sort('monto_cuota') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cobro_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($detalleCobros as $detalleCobro): ?>
            <tr>
                <td><?= $this->Number->format($detalleCobro->id) ?></td>
                <td><?= h($detalleCobro->materia) ?></td>
                <td><?= h($detalleCobro->monto_cuota) ?></td>
                <td><?= $detalleCobro->has('cobro') ? $this->Html->link($detalleCobro->cobro->id, ['controller' => 'Cobros', 'action' => 'view', $detalleCobro->cobro->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $detalleCobro->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $detalleCobro->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $detalleCobro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detalleCobro->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
