<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetalleCobro $detalleCobro
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Detalle Cobro'), ['action' => 'edit', $detalleCobro->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Detalle Cobro'), ['action' => 'delete', $detalleCobro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detalleCobro->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Detalle Cobros'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Detalle Cobro'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cobros'), ['controller' => 'Cobros', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cobro'), ['controller' => 'Cobros', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="detalleCobros view large-9 medium-8 columns content">
    <h3><?= h($detalleCobro->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Materia') ?></th>
            <td><?= h($detalleCobro->materia) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Monto Cuota') ?></th>
            <td><?= h($detalleCobro->monto_cuota) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cobro') ?></th>
            <td><?= $detalleCobro->has('cobro') ? $this->Html->link($detalleCobro->cobro->id, ['controller' => 'Cobros', 'action' => 'view', $detalleCobro->cobro->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($detalleCobro->id) ?></td>
        </tr>
    </table>
</div>
