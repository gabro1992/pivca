<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetalleCobro $detalleCobro
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Detalle Cobros'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Cobros'), ['controller' => 'Cobros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cobro'), ['controller' => 'Cobros', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="detalleCobros form large-9 medium-8 columns content">
    <?= $this->Form->create($detalleCobro) ?>
    <fieldset>
        <legend><?= __('Add Detalle Cobro') ?></legend>
        <?php
            echo $this->Form->control('materia');
            echo $this->Form->control('monto_cuota');
            echo $this->Form->control('cobro_id', ['options' => $cobros]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
