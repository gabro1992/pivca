<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StudentsProfessor $studentsProfessor
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Students Professor'), ['action' => 'edit', $studentsProfessor->student_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Students Professor'), ['action' => 'delete', $studentsProfessor->student_id], ['confirm' => __('Are you sure you want to delete # {0}?', $studentsProfessor->student_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Students Professors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Students Professor'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Professors'), ['controller' => 'Professors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Professor'), ['controller' => 'Professors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="studentsProfessors view large-9 medium-8 columns content">
    <h3><?= h($studentsProfessor->student_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Student') ?></th>
            <td><?= $studentsProfessor->has('student') ? $this->Html->link($studentsProfessor->student->name, ['controller' => 'Students', 'action' => 'view', $studentsProfessor->student->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Professor') ?></th>
            <td><?= $studentsProfessor->has('professor') ? $this->Html->link($studentsProfessor->professor->name, ['controller' => 'Professors', 'action' => 'view', $studentsProfessor->professor->id]) : '' ?></td>
        </tr>
    </table>
</div>
