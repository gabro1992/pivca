<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StudentsProfessor[]|\Cake\Collection\CollectionInterface $studentsProfessors
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Students Professor'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Professors'), ['controller' => 'Professors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Professor'), ['controller' => 'Professors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="studentsProfessors index large-9 medium-8 columns content">
    <h3><?= __('Students Professors') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('student_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('professor_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($studentsProfessors as $studentsProfessor): ?>
            <tr>
                <td><?= $studentsProfessor->has('student') ? $this->Html->link($studentsProfessor->student->name, ['controller' => 'Students', 'action' => 'view', $studentsProfessor->student->id]) : '' ?></td>
                <td><?= $studentsProfessor->has('professor') ? $this->Html->link($studentsProfessor->professor->name, ['controller' => 'Professors', 'action' => 'view', $studentsProfessor->professor->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $studentsProfessor->student_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $studentsProfessor->student_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $studentsProfessor->student_id], ['confirm' => __('Are you sure you want to delete # {0}?', $studentsProfessor->student_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
