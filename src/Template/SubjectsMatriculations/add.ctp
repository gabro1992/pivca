<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubjectsMatriculation $subjectsMatriculation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Subjects Matriculations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Matriculations'), ['controller' => 'Matriculations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Matriculation'), ['controller' => 'Matriculations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subjectsMatriculations form large-9 medium-8 columns content">
    <?= $this->Form->create($subjectsMatriculation) ?>
    <fieldset>
        <legend><?= __('Add Subjects Matriculation') ?></legend>
        <?php
            echo $this->Form->control('approved');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
