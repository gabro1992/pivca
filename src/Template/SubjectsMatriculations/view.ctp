<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubjectsMatriculation $subjectsMatriculation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Subjects Matriculation'), ['action' => 'edit', $subjectsMatriculation->subject_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subjects Matriculation'), ['action' => 'delete', $subjectsMatriculation->subject_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subjectsMatriculation->subject_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Subjects Matriculations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subjects Matriculation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Matriculations'), ['controller' => 'Matriculations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Matriculation'), ['controller' => 'Matriculations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subjectsMatriculations view large-9 medium-8 columns content">
    <h3><?= h($subjectsMatriculation->subject_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Subject') ?></th>
            <td><?= $subjectsMatriculation->has('subject') ? $this->Html->link($subjectsMatriculation->subject->name, ['controller' => 'Subjects', 'action' => 'view', $subjectsMatriculation->subject->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Matriculation') ?></th>
            <td><?= $subjectsMatriculation->has('matriculation') ? $this->Html->link($subjectsMatriculation->matriculation->description, ['controller' => 'Matriculations', 'action' => 'view', $subjectsMatriculation->matriculation->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Approved') ?></th>
            <td><?= $this->Number->format($subjectsMatriculation->approved) ?></td>
        </tr>
    </table>
</div>
