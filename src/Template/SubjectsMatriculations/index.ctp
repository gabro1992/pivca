<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubjectsMatriculation[]|\Cake\Collection\CollectionInterface $subjectsMatriculations
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Subjects Matriculation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Matriculations'), ['controller' => 'Matriculations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Matriculation'), ['controller' => 'Matriculations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subjectsMatriculations index large-9 medium-8 columns content">
    <h3><?= __('Subjects Matriculations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('subject_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('matriculation_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('approved') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subjectsMatriculations as $subjectsMatriculation): ?>
            <tr>
                <td><?= $subjectsMatriculation->has('subject') ? $this->Html->link($subjectsMatriculation->subject->name, ['controller' => 'Subjects', 'action' => 'view', $subjectsMatriculation->subject->id]) : '' ?></td>
                <td><?= $subjectsMatriculation->has('matriculation') ? $this->Html->link($subjectsMatriculation->matriculation->description, ['controller' => 'Matriculations', 'action' => 'view', $subjectsMatriculation->matriculation->id]) : '' ?></td>
                <td><?= $this->Number->format($subjectsMatriculation->approved) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $subjectsMatriculation->subject_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $subjectsMatriculation->subject_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $subjectsMatriculation->subject_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subjectsMatriculation->subject_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
