<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dependency $dependency
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dependency'), ['action' => 'edit', $dependency->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dependency'), ['action' => 'delete', $dependency->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dependency->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dependencies'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dependency'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dependencies view large-9 medium-8 columns content">
    <h3><?= h($dependency->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($dependency->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subject') ?></th>
            <td><?= $dependency->has('subject') ? $this->Html->link($dependency->subject->name, ['controller' => 'Subjects', 'action' => 'view', $dependency->subject->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dependency->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hijomateria') ?></th>
            <td><?= $this->Number->format($dependency->hijomateria) ?></td>
        </tr>
    </table>
</div>
