<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dependency $dependency
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dependency->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dependency->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Dependencies'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dependencies form large-9 medium-8 columns content">
    <?= $this->Form->create($dependency) ?>
    <fieldset>
        <legend><?= __('Edit Dependency') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('subject_id', ['options' => $subjects]);
            echo $this->Form->control('hijomateria');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
