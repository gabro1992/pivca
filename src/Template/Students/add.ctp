<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 */
?>
<script>
    $(document).on('click','#guardar',function(e) {
    //$("#guardar").click(function(e){
        e.preventDefault();
        $.ajax({
        method: "POST",
        url: "add",
        headers : {
      'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        data: { name: $("#name").val(),
                address: $("#address").val(),
                anho: $("#anho").val(),
                career_id: $("#career").val(),
                professor_id: $("#professor").val() },
        })
        .done(function( msg ) {
            if(msg == 'true'){
                alert('Estudiante agregado');
                $('#formulario').trigger("reset");
            }else{
                alert('NO SE PUDO AGREGAR AL ESTUDIANTE');
            }


        });
    });
</script>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Students'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Careers'), ['controller' => 'Careers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Career'), ['controller' => 'Careers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Professors'), ['controller' => 'Professors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Professor'), ['controller' => 'Professors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="students form large-9 medium-8 columns content">
    <?= $this->Form->create($student,['id'=>'formulario']) ?>

        <form>
            <legend>Agregar estudiante</legend>
            <?php
                echo $this->Form->control('name',['id'=>'name']);
                echo $this->Form->control('address',['id'=>'address']);
                echo $this->Form->control('year_ingress',['id'=>'anho']);
                echo $this->Form->control('career_id', ['options' => $careers,'id'=>'career']);
                echo $this->Form->control('professors._ids', ['options' => $professors,'id'=>'professor']);
            ?>
        </form>

    <button type="button" class="btn btn-primary float-right" id="guardar">Guardar</button>
    <!--<$this->Form->button(__('Submit')) ?>-->
    <?= $this->Form->end() ?>
</div>
