<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RequirementsSubject[]|\Cake\Collection\CollectionInterface $requirementsSubjects
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Requirements Subject'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Requirements'), ['controller' => 'Requirements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Requirement'), ['controller' => 'Requirements', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="requirementsSubjects index large-9 medium-8 columns content">
    <h3><?= __('Requirements Subjects') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('requirement_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subject_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($requirementsSubjects as $requirementsSubject): ?>
            <tr>
                <td><?= $requirementsSubject->has('requirement') ? $this->Html->link($requirementsSubject->requirement->name, ['controller' => 'Requirements', 'action' => 'view', $requirementsSubject->requirement->id]) : '' ?></td>
                <td><?= $requirementsSubject->has('subject') ? $this->Html->link($requirementsSubject->subject->name, ['controller' => 'Subjects', 'action' => 'view', $requirementsSubject->subject->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $requirementsSubject->requirement_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $requirementsSubject->requirement_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $requirementsSubject->requirement_id], ['confirm' => __('Are you sure you want to delete # {0}?', $requirementsSubject->requirement_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
