<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RequirementsSubject $requirementsSubject
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $requirementsSubject->requirement_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $requirementsSubject->requirement_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Requirements Subjects'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Requirements'), ['controller' => 'Requirements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Requirement'), ['controller' => 'Requirements', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="requirementsSubjects form large-9 medium-8 columns content">
    <?= $this->Form->create($requirementsSubject) ?>
    <fieldset>
        <legend><?= __('Edit Requirements Subject') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
