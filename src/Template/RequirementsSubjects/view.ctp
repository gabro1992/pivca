<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RequirementsSubject $requirementsSubject
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Requirements Subject'), ['action' => 'edit', $requirementsSubject->requirement_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Requirements Subject'), ['action' => 'delete', $requirementsSubject->requirement_id], ['confirm' => __('Are you sure you want to delete # {0}?', $requirementsSubject->requirement_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Requirements Subjects'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Requirements Subject'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Requirements'), ['controller' => 'Requirements', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Requirement'), ['controller' => 'Requirements', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="requirementsSubjects view large-9 medium-8 columns content">
    <h3><?= h($requirementsSubject->requirement_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Requirement') ?></th>
            <td><?= $requirementsSubject->has('requirement') ? $this->Html->link($requirementsSubject->requirement->name, ['controller' => 'Requirements', 'action' => 'view', $requirementsSubject->requirement->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subject') ?></th>
            <td><?= $requirementsSubject->has('subject') ? $this->Html->link($requirementsSubject->subject->name, ['controller' => 'Subjects', 'action' => 'view', $requirementsSubject->subject->id]) : '' ?></td>
        </tr>
    </table>
</div>
