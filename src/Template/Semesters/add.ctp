<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Semester $semester
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Semesters'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="semesters form large-9 medium-8 columns content">
    <?= $this->Form->create('Semester', array(
    'inputDefaults' => array(
        'label' => false,
        'div' => false,
        'name' => false
    ),
    'id' => 'dominio-form',
    'class' => 'smart-form',
    'novalidate' => 'novalidate'
	)) ?>
    <fieldset>
        <legend>Agregar Semestre</legend>

		<div class = "row">
        <section class= "col col-md-6">
				<label >Descripción</label>
				<label class="input">
					<?php echo $this->Form->input('', array('placeholder'=>'Introducza el nombre del dominio','name'=>'description','id'=>'description'));?>
			    </label>
		</section>
        </div>
        <div class = "row">
        <section class= "col col-md-6">
				<label >Periodo</label>
				<label class="input">
					<?php echo $this->Form->input('', array('placeholder'=>'Introducza el nombre del dominio','name'=>'period','id'=>'period'));?>
			    </label>
		</section>
        </div>
        <div class = "row">
        <section class= "col col-md-6">
				<label >Nombre</label>
				<label class="input">
					<?php echo $this->Form->input('', array('placeholder'=>'Introducza el nombre del dominio','name'=>'name','id'=>'name'));?>
			    </label>
		</section>
        </div>
        <div class = "row">
        <section class= "col col-md-6">
				<label >Observaciones</label>
				<label class="input">
					<?php echo $this->Form->input('', array('placeholder'=>'Introducza el nombre del dominio','name'=>'obs','id'=>'obs'));?>
			    </label>
		</section>
        </div>

    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
