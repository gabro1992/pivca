<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Student Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $address
 * @property int|null $year_ingress
 * @property int $career_id
 *
 * @property \App\Model\Entity\Career $career
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Professor[] $professors
 */
class Student extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'address' => true,
        'year_ingress' => true,
        'career_id' => true,
        'career' => true,
        'users' => true,
        'professors' => true
    ];
}
