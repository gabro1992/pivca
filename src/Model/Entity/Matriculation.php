<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Matriculation Entity
 *
 * @property int $id
 * @property string|null $description
 * @property int $career_id
 * @property int $student_id
 *
 * @property \App\Model\Entity\Career $career
 * @property \App\Model\Entity\Subject[] $subjects
 */
class Matriculation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'description' => true,
        'quantity' => true,
        'career_id' => true,
        'student_id' => true,
        'subject_id' => true,
        'career' => true,
        'student'=>true,
        'subjects' => true
    ];
}
