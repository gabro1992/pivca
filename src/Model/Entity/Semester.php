<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Semester Entity
 *
 * @property int $id
 * @property string|null $description
 * @property string|null $period
 * @property string|null $name
 * @property string $obs
 *
 * @property \App\Model\Entity\Subject[] $subjects
 */
class Semester extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'description' => true,
        'period' => true,
        'name' => true,
        'obs' => true,
        'subjects' => true
    ];
}
