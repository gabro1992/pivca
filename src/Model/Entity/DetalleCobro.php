<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DetalleCobro Entity
 *
 * @property int $id
 * @property string|null $materia
 * @property string|null $monto_cuota
 * @property int $cobro_id
 *
 * @property \App\Model\Entity\Cobro $cobro
 */
class DetalleCobro extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'materia' => true,
        'monto_cuota' => true,
        'cobro_id' => true,
        'cobro' => true
    ];
}
