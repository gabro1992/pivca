<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dependency Entity
 *
 * @property int $id
 * @property string|null $name
 * @property int $subject_id
 * @property int|null $hijomateria
 *
 * @property \App\Model\Entity\Subject $subject
 */
class Dependency extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'subject_id' => true,
        'hijomateria' => true,
        'subject' => true
    ];
}
