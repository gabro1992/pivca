<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Subject Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property int $career_id
 * @property int|null $price
 * @property int $semester_id
 *
 * @property \App\Model\Entity\Career $career
 * @property \App\Model\Entity\Semester $semester
 */
class Subject extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'career_id' => true,
        'price' => true,
        'semester_id' => true,
        'career' => true,
        'semester' => true,
        'requirements' => true
    ];
}
