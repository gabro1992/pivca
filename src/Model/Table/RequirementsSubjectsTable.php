<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RequirementsSubjects Model
 *
 * @property \App\Model\Table\RequirementsTable|\Cake\ORM\Association\BelongsTo $Requirements
 * @property \App\Model\Table\SubjectsTable|\Cake\ORM\Association\BelongsTo $Subjects
 *
 * @method \App\Model\Entity\RequirementsSubject get($primaryKey, $options = [])
 * @method \App\Model\Entity\RequirementsSubject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RequirementsSubject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RequirementsSubject|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RequirementsSubject saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RequirementsSubject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RequirementsSubject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RequirementsSubject findOrCreate($search, callable $callback = null, $options = [])
 */
class RequirementsSubjectsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('requirements_subjects');
        $this->setDisplayField('requirement_id');
        $this->setPrimaryKey(['requirement_id', 'subject_id']);

        $this->belongsTo('Requirements', [
            'foreignKey' => 'requirement_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Subjects', [
            'foreignKey' => 'subject_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['requirement_id'], 'Requirements'));
        $rules->add($rules->existsIn(['subject_id'], 'Subjects'));

        return $rules;
    }
}
