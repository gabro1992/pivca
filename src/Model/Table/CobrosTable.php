<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cobros Model
 *
 * @property \App\Model\Table\MatriculationsTable|\Cake\ORM\Association\BelongsTo $Matriculations
 * @property \App\Model\Table\DetalleCobrosTable|\Cake\ORM\Association\HasMany $DetalleCobros
 *
 * @method \App\Model\Entity\Cobro get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cobro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cobro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cobro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cobro saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cobro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cobro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cobro findOrCreate($search, callable $callback = null, $options = [])
 */
class CobrosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cobros');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Matriculations', [
            'foreignKey' => 'matriculation_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('DetalleCobros', [
            'foreignKey' => 'cobro_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('total')
            ->allowEmptyString('total');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['matriculation_id'], 'Matriculations'));

        return $rules;
    }
}
