<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DetalleCobros Model
 *
 * @property \App\Model\Table\CobrosTable|\Cake\ORM\Association\BelongsTo $Cobros
 *
 * @method \App\Model\Entity\DetalleCobro get($primaryKey, $options = [])
 * @method \App\Model\Entity\DetalleCobro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DetalleCobro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DetalleCobro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetalleCobro saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetalleCobro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DetalleCobro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DetalleCobro findOrCreate($search, callable $callback = null, $options = [])
 */
class DetalleCobrosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('detalle_cobros');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cobros', [
            'foreignKey' => 'cobro_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('materia')
            ->maxLength('materia', 45)
            ->allowEmptyString('materia');

        $validator
            ->scalar('monto_cuota')
            ->maxLength('monto_cuota', 45)
            ->allowEmptyString('monto_cuota');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cobro_id'], 'Cobros'));

        return $rules;
    }
}
