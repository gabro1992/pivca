<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StudentsProfessors Model
 *
 * @property \App\Model\Table\StudentsTable|\Cake\ORM\Association\BelongsTo $Students
 * @property \App\Model\Table\ProfessorsTable|\Cake\ORM\Association\BelongsTo $Professors
 *
 * @method \App\Model\Entity\StudentsProfessor get($primaryKey, $options = [])
 * @method \App\Model\Entity\StudentsProfessor newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StudentsProfessor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StudentsProfessor|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StudentsProfessor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StudentsProfessor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StudentsProfessor[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StudentsProfessor findOrCreate($search, callable $callback = null, $options = [])
 */
class StudentsProfessorsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('students_professors');
        $this->setDisplayField('student_id');
        $this->setPrimaryKey(['student_id', 'professor_id']);

        $this->belongsTo('Students', [
            'foreignKey' => 'student_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Professors', [
            'foreignKey' => 'professor_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['student_id'], 'Students'));
        $rules->add($rules->existsIn(['professor_id'], 'Professors'));

        return $rules;
    }
}
