<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SubjectsMatriculationsFixture
 */
class SubjectsMatriculationsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'subject_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'matriculation_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'approved' => ['type' => 'integer', 'length' => 1, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_subjects_has_matriculations_matriculations1_idx' => ['type' => 'index', 'columns' => ['matriculation_id'], 'length' => []],
            'fk_subjects_has_matriculations_subjects1_idx' => ['type' => 'index', 'columns' => ['subject_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['subject_id', 'matriculation_id'], 'length' => []],
            'fk_subjects_has_matriculations_matriculations1' => ['type' => 'foreign', 'columns' => ['matriculation_id'], 'references' => ['matriculations', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_subjects_has_matriculations_subjects1' => ['type' => 'foreign', 'columns' => ['subject_id'], 'references' => ['subjects', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'subject_id' => 1,
                'matriculation_id' => 1,
                'approved' => 1
            ],
        ];
        parent::init();
    }
}
