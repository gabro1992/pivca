<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RequirementsSubjectsFixture
 */
class RequirementsSubjectsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'requirement_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'subject_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_requirements_has_subjects_subjects1_idx' => ['type' => 'index', 'columns' => ['subject_id'], 'length' => []],
            'fk_requirements_has_subjects_requirements1_idx' => ['type' => 'index', 'columns' => ['requirement_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['requirement_id', 'subject_id'], 'length' => []],
            'fk_requirements_has_subjects_requirements1' => ['type' => 'foreign', 'columns' => ['requirement_id'], 'references' => ['requirements', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_requirements_has_subjects_subjects1' => ['type' => 'foreign', 'columns' => ['subject_id'], 'references' => ['subjects', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'requirement_id' => 1,
                'subject_id' => 1
            ],
        ];
        parent::init();
    }
}
