<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DetalleCobrosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DetalleCobrosTable Test Case
 */
class DetalleCobrosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DetalleCobrosTable
     */
    public $DetalleCobros;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DetalleCobros',
        'app.Cobros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DetalleCobros') ? [] : ['className' => DetalleCobrosTable::class];
        $this->DetalleCobros = TableRegistry::getTableLocator()->get('DetalleCobros', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DetalleCobros);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
