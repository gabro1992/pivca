<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequirementsSubjectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequirementsSubjectsTable Test Case
 */
class RequirementsSubjectsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequirementsSubjectsTable
     */
    public $RequirementsSubjects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequirementsSubjects',
        'app.Requirements',
        'app.Subjects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequirementsSubjects') ? [] : ['className' => RequirementsSubjectsTable::class];
        $this->RequirementsSubjects = TableRegistry::getTableLocator()->get('RequirementsSubjects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequirementsSubjects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
