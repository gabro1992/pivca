<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StudentsProfessorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StudentsProfessorsTable Test Case
 */
class StudentsProfessorsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\StudentsProfessorsTable
     */
    public $StudentsProfessors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.StudentsProfessors',
        'app.Students',
        'app.Professors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StudentsProfessors') ? [] : ['className' => StudentsProfessorsTable::class];
        $this->StudentsProfessors = TableRegistry::getTableLocator()->get('StudentsProfessors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StudentsProfessors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
